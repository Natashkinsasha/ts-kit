import * as chai from "chai";
import * as supertest from "supertest";
import { SERVER_URL } from "./util";

describe("Test info endpoint", () => {
    const {expect} = chai;

    describe("#GET /api/v1/info/time", () => {
        it("should return server time", (done) => {
            supertest(SERVER_URL)
                .get("/api/v1/info/time")
                .expect(200)
                .then((res: any) => {
                    expect(res).to.have.property("body").to.be.a("string");
                    done();
                })
                .catch(done);
        });
    });
});
