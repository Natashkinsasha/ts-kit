import * as config from "node-config-env-value";

const PORT = config.get("PORT");
const SERVER_URL = `http://localhost:${PORT}`;
export { SERVER_URL };
