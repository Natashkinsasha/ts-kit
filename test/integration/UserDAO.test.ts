import * as chai from "chai";
import * as faker from "faker";
import { Db } from "mongodb";
import shortid = require("shortid");
import MongoDB from "../../src/components/MongoDB";
import UserDAO from "../../src/DAO/UserDAO";
import UpdateUserDTO from "../../src/DTO/UpdateUserDTO";
import IUser from "../../src/model/IUser";
import User from "../../src/model/User";

describe("Test UserDAO", () => {
    const {expect} = chai;
    let db: Db;
    let userDAO: UserDAO;

    before(() => {
        db = new MongoDB().get();
        userDAO = new UserDAO({db});
    });

    after((done) => {
        db.close(() => {
            done();
        });
    });

    describe("create", () => {
        it("1", (done) => {
            const nickname: string = faker.name.findName();
            const user: IUser = {nickname};
            userDAO
                .create({user})
                .then((user: User) => {
                    expect(user).to.have.property("id").to.be.a("string");
                    expect(user).to.have.property("nickname", nickname);
                    done();
                })
                .catch(done);

        });
    });

    describe("update", () => {
        it("1", (done) => {
            const nickname: string = faker.name.findName();
            const user: IUser = {nickname: faker.name.findName()};
            userDAO
                .create({user})
                .then((user: User) =>
                    userDAO.update({id: user.id, user: new UpdateUserDTO({nickname})}))
                .then((user: User) => {
                    expect(user).to.have.property("id").to.be.a("string");
                    expect(user).to.have.property("nickname", nickname).to.be.a("string");
                    done();
                })
                .catch(done);
        });
    });

    describe("findByDeviceIdAndSave", () => {

        it("1", (done) => {
            const deviceId: string = shortid.generate();
            const nickname: string = faker.name.findName();
            const user: IUser = {nickname, deviceId};
            userDAO
                .findByDeviceIdAndSave({deviceId, user})
                .then((user: User) => {
                    expect(user).to.have.property("id").to.be.a("string");
                    expect(user).to.have.property("deviceId", deviceId).to.be.a("string");
                    expect(user).to.have.property("nickname", nickname).to.be.a("string");
                    done();
                })
                .catch(done);
        });

        it("2", (done) => {
            const deviceId: string = shortid.generate();
            const nickname: string = faker.name.findName();
            const user: IUser = {nickname: faker.name.findName(), deviceId};
            userDAO
                .findByDeviceIdAndSave({deviceId, user})
                .then(() => {
                    const user: IUser = {nickname};
                    return userDAO
                        .findByDeviceIdAndSave({deviceId, user});
                })
                .then((user: User) => {
                    expect(user).to.have.property("id").to.be.a("string");
                    expect(user).to.have.property("deviceId", deviceId).to.be.a("string");
                    expect(user).to.have.property("nickname", nickname).to.be.a("string");
                    done();
                })
                .catch(done);
        });

    });

});
