import * as chai from "chai";
import * as request from "supertest";

import App from "../../src/components/App";
import InfoController from "../../src/controller/InfoController";
import BasicRouter from "../../src/router/BasicRouter";
import InfoRouter from "../../src/router/InfoRouter";
import InfoService from "../../src/service/InfoService";

describe("Test InfoRouter", () => {
    const {expect} = chai;
    const infoService = new InfoService();
    const infoController = new InfoController({infoService});
    const infoRouter = new InfoRouter({infoController});
    const basicRouter = new BasicRouter({infoRouter});
    const app = new App({basicRouter}).get();

    describe("#GET /api/v1/info/time", () => {
        it("should return server time", () =>
            request(app)
                .get("/api/v1/info/time")
                .expect(200)
                .then((res: any) => {
                    expect(res).to.have.property("body").to.be.a("string");
                }));
    });
});
