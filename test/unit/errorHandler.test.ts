import * as chai from "chai";
import * as  shortid from "shortid";
import * as  sinon from "sinon";
import * as sinonChai from "sinon-chai";

import { mockReq, mockRes } from "sinon-express-mock";
import errorHandler from "../../src/middleware/errorHandler";

describe("Test errorHandler", () => {
    chai.use(sinonChai);
    const {expect} = chai;

    it("should calls status with 500 and json", (done) => {
        const message = shortid.generate();
        const res = mockRes();
        errorHandler()(new TypeError(message), mockReq(), res, sinon.spy())
            .then(() => {
                expect(res.status).to.be.calledOnce;
                expect(res.status).to.be.calledWith(500);
                expect(res.json).to.be.calledOnce;
                expect(res.json.getCall(0).args[0]).to.have.property("name", "TypeError");
                expect(res.json.getCall(0).args[0]).to.have.property("message", message);
                expect(res.json.getCall(0).args[0]).to.have.property("code");
                expect(res.json.getCall(0).args[0]).to.have.property("failureId");
                done();
            })
            .catch(done);
    });

});
