import * as chai from "chai";
import * as  sinon from "sinon";
import * as sinonChai from "sinon-chai";
import { mockReq, mockRes } from "sinon-express-mock";

import InfoController from "../../src/controller/InfoController";
import InfoService from "../../src/service/InfoService";

describe("Test errorHandler", () => {
    chai.use(sinonChai);
    const {expect} = chai;
    const infoService = new InfoService();
    let infoController: InfoController;
    let InfoServiceMock: any;

    beforeEach(() => {
        InfoServiceMock = sinon.mock(infoService);
        infoController = new InfoController({infoService});
    });

    afterEach(() => {
        InfoServiceMock.restore();
    });

    describe("#getServerTime", () => {
        it("should calls status with 200 and json", (done) => {
            const res = mockRes();
            InfoServiceMock
                .expects("getServerTime")
                .once()
                .returns(Promise.resolve(new Date()));

            infoController
                .time(mockReq(), res, sinon.spy())
                .then(() => {
                    expect(res.status).to.be.calledWith(200);
                    expect(res.status).to.be.calledOnce;
                    expect(res.json).to.be.calledOnce;
                    done();
                })
                .catch(done);
        });

        it("should calls next with error", (done) => {
            const next = sinon.spy();
            const error = new TypeError();
            InfoServiceMock
                .expects("getServerTime")
                .once()
                .returns(Promise.reject(error));

            infoController
                .time(mockReq(), mockRes(), next)
                .then(() => {
                    expect(next.calledOnce).to.be.true;
                    expect(next.calledWith(error)).to.be.true;
                    done();
                })
                .catch(done);
        });
    });
});
