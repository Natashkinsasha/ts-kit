import UserDAO from "../DAO/UserDAO";
import UpdateUserDTO from "../DTO/UpdateUserDTO";
import IUser from "../model/IUser";
import User from "../model/User";

export default class UserService {

    private readonly userDAO: UserDAO;

    public constructor({userDAO}: {userDAO: UserDAO}) {
        this.userDAO = userDAO;
    }

    public findById = (id: string): Promise<IUser | undefined> =>
        this.userDAO.findById({id})

    public update = ({id, user}: {id: string, user: UpdateUserDTO}): Promise<User | undefined> =>
        this.userDAO.update({id, user})

    public findByDeviceIdAndSave = ({deviceId, user}: {deviceId: string, user: IUser}): Promise<User>   =>
        this.userDAO.findByDeviceIdAndSave({deviceId, user})

}
