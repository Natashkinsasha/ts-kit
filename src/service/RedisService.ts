import * as Promise from "bluebird";
import RedisDAO from "../DAO/RedisDAO";

export default class RedisService {
    private readonly redisDAO: RedisDAO;
    public constructor({ redisDAO }: {redisDAO: RedisDAO}) {
        this.redisDAO = redisDAO;
    }
    public clear = (): Promise<string> => this.redisDAO.clear();

    public remove = ({ key }: {key: string}): Promise<string> => this.redisDAO.remove(key);
}
