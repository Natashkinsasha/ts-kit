import * as Promise from "bluebird";
import JWTUserDTO from "../DTO/JWTUserDTO";
import IUser from "../model/IUser";
import User from "../model/User";
import JWTService from "./JwtService";
import UserService from "./UserService";

export default class AuthService {

    private readonly jwtService: JWTService;
    private readonly userService: UserService;

    public constructor({jwtService, userService}: { jwtService: JWTService, userService: UserService }) {
        this.jwtService = jwtService;
        this.userService = userService;
    }

    public authAnonymous = ({deviceId}: { deviceId: string }): Promise<{token: string, user: User}> => {
        const user: IUser = {
            deviceId,
        };
        return this.userService
            .findByDeviceIdAndSave({deviceId, user})
            .then((user: User) =>
                Promise
                    .props({token: this.jwtService.create(new JWTUserDTO(user)), user}));
    }

}
