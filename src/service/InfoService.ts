import * as Promise from "bluebird";

export default class InfoService {
    public getServerTime = (): Promise<Date> =>
        Promise
            .try(() =>
                new Date())
}
