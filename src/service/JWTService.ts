import * as Promise from "bluebird";
import { classToPlain } from "class-transformer";
import * as jwt from "jsonwebtoken";
import * as config from "node-config-env-value";

import JWTUserDTO from "../DTO/JWTUserDTO";
import Payload from "../model/Payload";

export default class JwtService {

    private readonly secretKey: string;

    public constructor() {
        this.secretKey = config.get("AUTH.SECRET_KEY");
    }

    public create = (user: JWTUserDTO): Promise <string> =>
        Promise
            .try(() =>
                jwt.sign(classToPlain(new Payload({user})), this.secretKey))

    public validate = (token: string): Promise<Payload> =>
        Promise
            .try(() =>
                jwt.verify(token, this.secretKey) as Payload)

}
