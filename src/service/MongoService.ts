import * as Promise from "bluebird";
import { DeleteWriteOpResultObject } from "mongodb";
import MongoDAO from "../DAO/MongoDAO";

export default class MongoService {
    private readonly mongoDAO: MongoDAO;

    public constructor({mongoDAO}: { mongoDAO: MongoDAO }) {
        this.mongoDAO = mongoDAO;
    }

    public clear = ({collection}: { collection: string }): Promise<DeleteWriteOpResultObject[]>  => {
        if (collection) {
            return this.mongoDAO.clear(collection).then((isDelete: DeleteWriteOpResultObject) => ([isDelete]));
        }
        return this.mongoDAO.clearAll();
    }

    public drop = ({collection}: { collection: string }): Promise<boolean[]> => {
        if (collection) {
            return this.mongoDAO.drop(collection).then((isDrop: boolean) => ([isDrop]));
        }
        return this.mongoDAO.dropAll();
    }

    public remove = ({id, collection}: { id: string, collection: string }): Promise<DeleteWriteOpResultObject> =>
        this.mongoDAO.remove(id, collection)
}
