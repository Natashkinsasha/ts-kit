import * as Promise from "bluebird";
import { Db, DeleteWriteOpResultObject, ObjectID } from "mongodb";

export default class MongoDAO {

    private readonly db: Db;

    public constructor({db}: { db: Db }) {
        this.db = db;
    }

    public clearAll = (): Promise<DeleteWriteOpResultObject[]> =>
        this.db.listCollections({name: {$regex: /^((?!system).)*$/i}})
            .toArray()
            .then((collections) =>
                Promise.map(collections, (collection: any) => this.db.collection(collection.name).deleteMany({})))

    public clear = (collection: string): Promise<DeleteWriteOpResultObject> => this.db.collection(collection).deleteMany({});

    public drop = (collection: string): Promise<boolean> => this.db.dropCollection(collection);

    public dropAll = (): Promise<boolean[]> =>
        this.db.listCollections({name: {$regex: /^((?!system).)*$/i}})
            .toArray()
            .then((collections) =>
                Promise.map(collections, (collection: any) => this.db.dropCollection(collection.name)))

    public remove = (id: string, collection: string): Promise<DeleteWriteOpResultObject> =>
        this.db.collection(collection)
            .deleteOne({_id: new ObjectID(id)})
}
