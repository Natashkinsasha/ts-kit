import * as Promise from "bluebird";
import * as IOredis from "ioredis";

export default class RedisDao {
    private readonly redisClient: IOredis.Redis;
    public constructor({ redisClient }: {redisClient: IOredis.Redis}) {
        this.redisClient = redisClient;
    }

    public clear = (): Promise<string> => this.redisClient.flushdb();

    public remove = (key: string): Promise<string> =>
        this.redisClient.del(key).return(key)

    public save = (key: string, value: object | string | boolean | number): Promise<object | string | boolean | number>  =>
        this.redisClient.set(key, JSON.stringify(value)).return(value)

    public get = (key: string): Promise<undefined | object | string | boolean | number> =>
        this.redisClient.get(key).then((json: string) => (json && JSON.parse(json)))

    public rename = (key: string, newKey: string): Promise<string> => this.redisClient.rename(key, newKey);

    public isExist = (key: string): Promise<boolean> => this.redisClient.exists(key).then(Boolean);
}
