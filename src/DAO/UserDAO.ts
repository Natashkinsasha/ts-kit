import { Db, FindAndModifyWriteOpResultObject, InsertOneWriteOpResult, ObjectId } from "mongodb";
import UpdateUserDTO from "../DTO/UpdateUserDTO";
import IUserEntity from "../entity/IUserEntity";
import ConsistencyError from "../error/ConsistencyError";
import IUser from "../model/IUser";
import User from "../model/User";

export default class UserDAO {

    private readonly db: Db;

    public constructor({db}: { db: Db }) {
        this.db = db;
    }

    public create = ({user}: { user: IUser }): Promise<User | undefined> =>
        this.db.collection("users")
            .insertOne(user)
            .then((result: InsertOneWriteOpResult) =>
                new User({...user, _id: result.insertedId}))

    public findById = ({id}: { id: string }): Promise<IUser | undefined> =>
        this.db.collection("users")
            .findOne({_id: new ObjectId(id)})
            .then((user: IUserEntity) =>
                user && new User(user))

    public update = ({id, user}: { id: string, user: UpdateUserDTO }): Promise<User | undefined> =>
        this.db.collection("users")
            .findOneAndUpdate({_id: new ObjectId(id)}, {$set: user}, {returnOriginal: false})
            .then((result: FindAndModifyWriteOpResultObject<IUserEntity>) =>
                result.value && new User(result.value))

    public findByDeviceIdAndSave = ({deviceId, user}: { deviceId: string, user: IUser }): Promise<User> =>
        this.db.collection("users")
            .findOneAndUpdate({deviceId}, { $set: user }, {upsert: true, returnOriginal: false})
            .then((result: FindAndModifyWriteOpResultObject<IUserEntity>) => {
                if (result.value) {
                    return new User(result.value);
                }
                throw new ConsistencyError({});
            })

}
