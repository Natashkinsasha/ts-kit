import * as express from "express";
import InfoController from "../controller/InfoController";

export default class InfoRouter {
    private readonly router: express.Router;

    public constructor({infoController}: { infoController: InfoController }) {
        this.router = express.Router().get("/time", infoController.time);
    }

    public get = (): express.Router =>
        this.router
}
