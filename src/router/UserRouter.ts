import * as express from "express";
import UserController from "../controller/UserController";
import auth from "../middleware/auth";
import { validatePut } from "../validator/userValidator";

export default class UserRouter {
    private readonly router: express.Router;

    public constructor({userController}: { userController: UserController }) {
        this.router = express.Router()
            .put("/", validatePut, auth(), userController.update);
    }

    public get = (): express.Router =>
        this.router

}
