import * as express from "express";
import AuthController from "../controller/AuthController";
import { validateGetAnonymous } from "../validator/authValidor";

export default class AuthRouter {
    private readonly router: express.Router;

    public constructor({authController}: { authController: AuthController }) {
        this.router = express.Router()
            /**
             * @api {get} api/v1/auth/anonymous Anonymous auth
             * @apiName AnonymousAuth
             * @apiGroup Auth
             * @apiVersion 1.0.0
             * @apiPermission none
             *
             * @apiParam (params) {String} deviceId Device id
             *
             * @apiSuccess {User} user User
             * @apiSuccess {String} token Token
             *
             * @apiSuccessExample {json} Success-Response:
             *     HTTP/1.1 200 OK
             *     {
             *       "user": User,
             *       "token": String
             *     }
             */
            .get("/anonymous", validateGetAnonymous, authController.authAnonymous);
    }

    public get = (): express.Router =>
        this.router

}
