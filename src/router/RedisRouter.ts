import * as express from "express";
import RedisController from "../controller/RedisController";

export default class RedisRouter {
    private readonly router: express.Router;

    public constructor({redisController}: { redisController: RedisController }) {
        this.router = express.Router()
            .delete("/clear", redisController.clear)
            .delete("/remove", redisController.remove);
    }

    public get = (): express.Router =>
        this.router

}
