import * as express from "express";
import MongoController from "../controller/MongoController";

export default class MongoRouter {
    private readonly router: express.Router;

    public constructor({mongoController}: { mongoController: MongoController }) {
        this.router = express.Router()
            .delete("/clear", mongoController.clear)
            .delete("/drop", mongoController.drop)
            .delete("/remove", mongoController.remove);
    }

    public get = (): express.Router =>
        this.router

}
