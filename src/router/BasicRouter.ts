import * as express from "express";
import InfoRouter from "../router/InfoRouter";
import AuthRouter from "./AuthRouter";
import MongoRouter from "./MongoRouter";
import RedisRouter from "./RedisRouter";
import UserRouter from "./UserRouter";

interface IBasicRouterConstructor {
    infoRouter?: InfoRouter;
    redisRouter?: RedisRouter;
    mongoRouter?: MongoRouter;
    userRouter?: UserRouter;
    authRouter?: AuthRouter;
}

export default class BasicRouter {
    private readonly router: express.Router;
    public constructor({infoRouter, redisRouter, mongoRouter, userRouter, authRouter}: IBasicRouterConstructor) {
        this.router = express.Router();
        infoRouter && this.router.use("/info", infoRouter.get());
        redisRouter && this.router.use("/redis", redisRouter.get());
        mongoRouter && this.router.use("/mongo", mongoRouter.get());
        userRouter && this.router.use("/user", userRouter.get());
        authRouter && this.router.use("/auth", authRouter.get());
    }

    public get = (): express.Router =>
        this.router
}
