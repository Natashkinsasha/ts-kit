import errorCode from "./errorCodes";
import LogicError from "./LogicError";

export default class ConsistencyError extends LogicError {
    public constructor({ message = "Consistency error", code = errorCode.CONSISTENCY_ERROR }) {
        super({ message, code });
    }
}
