import errorCode from "./errorCodes";
import LogicError from "./LogicError";

export default class ValidationError extends LogicError {
  public result: any;
  public constructor({ message = "Validation error", code = errorCode.VALIDATION_ERROR, result = {} }: {message?: string, code?: string, result?: any}) {
    super({ message, code });
    this.result = result;
  }
}
