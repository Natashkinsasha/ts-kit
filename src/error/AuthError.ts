import errorCode from "./errorCodes";
import LogicError from "./LogicError";

export default class AuthError extends LogicError {
    public constructor({ message = "Auth error", code = errorCode.AUTH_ERROR }) {
        super({ message, code });
    }
}
