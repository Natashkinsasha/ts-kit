import "bluebird-global";

import App from "./components/App";
import HttpServer from "./components/HttpServer";
import logger from "./components/logger";
import MongoDB from "./components/MongoDB";

import RedisClient from "./components/RedisClient";
import AuthController from "./controller/AuthController";
import InfoController from "./controller/InfoController";
import MongoController from "./controller/MongoController";
import RedisController from "./controller/RedisController";
import UserController from "./controller/UserController";
import MongoDAO from "./DAO/MongoDAO";
import RedisDAO from "./DAO/RedisDAO";
import UserDAO from "./DAO/UserDAO";
import AuthRouter from "./router/AuthRouter";
import BasicRouter from "./router/BasicRouter";
import InfoRouter from "./router/InfoRouter";
import MongoRouter from "./router/MongoRouter";
import RedisRouter from "./router/RedisRouter";
import UserRouter from "./router/UserRouter";
import AuthService from "./service/AuthService";
import InfoService from "./service/InfoService";
import JWTService from "./service/JWTService";
import MongoService from "./service/MongoService";
import RedisService from "./service/RedisService";
import UserService from "./service/UserService";

process.on("uncaughtException", (err: any) => {
    logger.error(err.stack);
});

process.on("unhandledRejection", (err) => {
    logger.error(err.stack);
});
const db = new MongoDB().get();
const redisClient = new RedisClient().get();
const mongoDAO = new MongoDAO({db});
const userDAO = new UserDAO({db});
const redisDAO = new RedisDAO({redisClient});
const userService = new UserService({userDAO});
const jwtService = new JWTService();
const authService = new AuthService({userService, jwtService});
const redisService = new RedisService({redisDAO});
const mongoService = new MongoService({mongoDAO});
const infoService = new InfoService();
const userController = new UserController({userService});
const authController = new AuthController({authService});
const mongoController = new MongoController({mongoService});
const redisController = new RedisController({redisService});
const infoController = new InfoController({infoService});
const infoRouter = new InfoRouter({infoController});
const mongoRouter = new MongoRouter({mongoController});
const redisRouter = new RedisRouter({redisController});
const userRouter = new UserRouter({userController});
const authRouter = new AuthRouter({authController});
const basicRouter = new BasicRouter({infoRouter, mongoRouter, redisRouter, authRouter, userRouter});
const app = new App({basicRouter});
const httpServer = new HttpServer({app});

httpServer.start();
