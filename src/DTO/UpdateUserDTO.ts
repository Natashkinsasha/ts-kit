import IUser from "../model/IUser";

export default class JWTUserDTO {

    public nickname?: string;
    public constructor(user: IUser) {
        this.nickname = user.nickname;
    }

}
