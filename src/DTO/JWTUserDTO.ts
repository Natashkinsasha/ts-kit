import User from "../model/User";

export default class JWTUserDTO {

    public id: string;
    public roles?: string[];
    public constructor(user: User) {
        this.id = user.id;
        this.roles = user.roles;
    }

}
