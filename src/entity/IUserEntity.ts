import { ObjectId } from "mongodb";
import IUser from "../model/IUser";

export default interface IUserEntity extends IUser {
    _id: ObjectId;
}
