import JWTUserDTO from "../DTO/JWTUserDTO";

export default class Payload {
    public user: JWTUserDTO;

    public constructor({user}: {user: JWTUserDTO}) {
        this.user = user;
    }
}
