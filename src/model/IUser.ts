export default interface IUser {
    nickname?: string;
    deviceId?: string;
    roles?: string[];
}
