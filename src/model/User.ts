import IUserEntity from "../entity/IUserEntity";
import IUser from "./IUser";

export default class User implements IUser {
    public id: string;
    public deviceId?: string;
    public nickname?: string;
    public roles?: string[];

    public constructor(user: IUserEntity) {
        this.id = user._id.toHexString();
        this.deviceId = user.deviceId;
        this.nickname = user.nickname;
        this.roles = user.roles;
    }

}
