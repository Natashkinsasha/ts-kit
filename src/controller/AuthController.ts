import * as Promise from "bluebird";
import * as express from "express";
import { Response } from "express-serve-static-core";
import User from "../model/User";
import AuthService from "../service/AuthService";

export default class AuthController {
    private readonly authService: AuthService;

    public constructor({authService}: { authService: AuthService }) {
        this.authService = authService;
    }

    public authAnonymous = (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void | Response> => {
        const deviceId: string = req.params.deviceId;
        return this.authService
            .authAnonymous({deviceId})
            .then(({token, user}: { token: string, user: User }) =>
                res.json({token, user}))
            .catch(next);
    }

}
