import * as express from "express";
import UpdateUserDTO from "../DTO/UpdateUserDTO";
import ConsistencyError from "../error/ConsistencyError";
import Payload from "../model/Payload";
import User from "../model/User";
import UserService from "../service/UserService";

export default class UserController {

    private readonly userService: UserService;

    public constructor({userService}: { userService: UserService }) {
        this.userService = userService;
    }

    public update = (req: express.Request & { payload: Payload }, res: express.Response, next: express.NextFunction) => {
        const user: UpdateUserDTO = new UpdateUserDTO(req.body);
        const id: string = req.payload.user.id;
        return this.userService.update({id, user})
            .then((user: User) => {
                if (!user) {
                    throw new ConsistencyError({message: "Not find user"});
                }
                return res.json(user);
            })
            .catch(next);
    }
}
