import * as express from "express";
import MongoService from "../service/MongoService";

export default class MongoController {
    private readonly mongoService: MongoService;

    public constructor({mongoService}: { mongoService: MongoService }) {
        this.mongoService = mongoService;
    }

    public clear = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const {collection} = req.query;
        return this.mongoService
            .clear({collection})
            .then(() => res.status(200).end())
            .catch(next);
    }

    public drop = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const {collection} = req.query;
        return this.mongoService
            .drop({collection})
            .then(() => res.status(200).end())
            .catch(next);
    }

    public remove = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const {id, collection} = req.query;
        return this.mongoService
            .remove({id, collection})
            .then(() => res.status(200).end())
            .catch(next);
    }
}
