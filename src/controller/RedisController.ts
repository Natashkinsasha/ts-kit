import * as express from "express";
import RedisService from "../service/RedisService";

export default class RedisController {
    private readonly redisService: RedisService;

    public constructor({redisService}: { redisService: RedisService }) {
        this.redisService = redisService;
    }

    public clear = (req: express.Request, res: express.Response, next: express.NextFunction) =>
        this.redisService
            .clear()
            .then(() => res.status(200).end())
            .catch(next)

    public remove = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        const {key} = req.query;
        return this.redisService.remove({key})
            .then(() => res.status(200).end())
            .catch(next);
    }
}
