import * as express from "express";

import InfoService from "../service/InfoService";

export default class InfoController {
    private readonly infoService: InfoService;

    public constructor({infoService}: { infoService: InfoService }) {
        this.infoService = infoService;
    }

    public time = (req: express.Request, res: express.Response, next: express.NextFunction) =>
        this.infoService
            .getServerTime()
            .then((time: Date) => (res.status(200).json(time)))
            .catch(next)
}
