import * as config from "node-config-env-value";
import * as performance from "performance-nodejs";
import * as shortid from "shortid";

import log from "../components/logger";

const perfLogs = config.get("PERF_LOGS");

export default {
    start: () => {
        if (perfLogs) {
            const nodeId = shortid.generate();
            return performance((data: any) =>
                log.log("prof", {
                            id: nodeId,
                            logType: "Performance Data",
                            performanceData: data,
                        }.toString())
            ,                  "MB", 5000);
        }
        return true;
    },
};
