import * as IOredis from "ioredis";
import * as config from "node-config-env-value";

import logger from "./logger";

export default class RedisClient {
    private readonly redisClient: IOredis.Redis;
    public constructor() {
        const REDIS_URL = config.get("REDIS_URL");
        const redisClient: IOredis.Redis = new IOredis(REDIS_URL);
        redisClient.on("connect", () => {
            logger.info("StartAloneRedis connection open on REDIS_URL");
        });

        redisClient.on("ready", () => {
            logger.info("StartAloneRedis ready");
        });

        redisClient.on("error", (err: any) => {
            logger.error("StartAloneRedis error: " + err.stack);
        });

        redisClient.on("close", () => {
            logger.warn("StartAloneRedis close");
        });

        redisClient.on("reconnecting", () => {
            logger.warn("StartAloneRedis reconnecting");
        });

        redisClient.on("end", () => {
            logger.warn("StartAloneRedis end");
        });

        redisClient.on("select", () => {
            logger.warn("StartAloneRedis select");
        });

        this.redisClient = redisClient;
    }

    public get = () =>
        this.redisClient
}
