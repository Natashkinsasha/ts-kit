import * as deasync from "deasync";
import * as Redis from "ioredis";
import * as config from "node-config-env-value";

import logger from "../components/logger";

export default class RedisCluster {
    public constructor(NODE = config.get("REDIS.NODE")) {
        const REDIS_URL = config.get("REDIS_URL");
        const redisCluster = new Redis(REDIS_URL);

        redisCluster.on("connect", () => {
            logger.info(`RedisCluster connection open on ${REDIS_URL}`);
        });

        redisCluster.on("error", (err: any) => {
            logger.error(`RedisCluster error: ${err.stack}`);
        });

        redisCluster.on("close", () => {
            logger.warn("RedisCluster close");
        });

        redisCluster.on("reconnecting", () => {
            logger.warn("RedisCluster reconnecting");
        });

        redisCluster.on("end", () => {
            logger.warn("RedisCluster end");
        });

        redisCluster.on("+node", () => {
            logger.info("RedisCluster +node");
        });

        redisCluster.on("-node", () => {
            logger.info("RedisCluster -node");
        });

        redisCluster.on("node error", (err) => {
            logger.error(`RedisCluster node error: ${err.stack}`);
        });

        let done = false;
        redisCluster.on("ready", () => {
            done = true;
            logger.info("RedisCluster ready");
        });
        deasync.loopWhile(() => (!done));

        return redisCluster;
    }
}
