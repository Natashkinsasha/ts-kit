import * as config from "node-config-env-value";
import * as winston from "winston";

const ENV = config.get("NODE_ENV");
const LOG_LEVEL = config.get("LOG_LEVEL");

const rewriters = [
    (level: string, msg: string, meta: object) => ({ ...meta, env: ENV, timeStamp: new Date() }),
];

const levels = {
    // tslint:disable-next-line: object-literal-sort-keys
    error: 0, warn: 1, state: 2, prof: 3, info: 4, debug: 5,
};

const colors = {
    // tslint:disable-next-line: object-literal-sort-keys
    error: "red", warn: "cyan", state: "green", prof: "blue", info: "yellow", debug: "gray",
};

winston.addColors(colors);

const transports = [
    new winston.transports.Console({
        colorize: true,
        level: LOG_LEVEL,
        timestamp: true,
    }),
];

export default new winston.Logger({
    levels,
    rewriters,
    transports,
});
