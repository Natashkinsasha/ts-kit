import * as Promise from "bluebird";
import * as deasync from "deasync";
import { MongoClient } from "mongodb";
import * as config from "node-config-env-value";

import logger from "./logger";

export default class MongoDb {
    private readonly db: any;
    public constructor() {
        const url: string = config.get("MONGODB_URI");
        const db = deasync(MongoClient.connect)(url, { promiseLibrary: Promise });
        db.on("authenticated", () => {
            logger.info("Db authenticated");
        });
        db.on("close", () => {
            logger.warn("Db close");
        });

        db.on("error", () => {
            logger.error("Db error");
        });

        db.on("parseError", () => {
            logger.error("Db parseError");
        });

        db.on("fullsetup", () => {
            logger.log("state", "Db fullsetup");
        });

        db.on("reconnect", () => {
            logger.warn("Db reconnect");
        });

        db.on("timeout", () => {
            logger.warn("Db timeout");
        });
        this.db = db;
    }

    public get = () =>
        this.db
}
