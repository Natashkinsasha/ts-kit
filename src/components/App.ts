import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as helmet from "helmet";
import * as queryParser from "query-parser-express";

import errorHandler from "../middleware/errorHandler";
import logger from "../middleware/logger";
import validator from "../middleware/validator";
import BasicRouter from "../router/BasicRouter";

export default class App {
    private readonly app: any;
    public constructor({basicRouter}: {basicRouter: BasicRouter}) {
        const app = express();
        app.use(helmet());
        app.use(cors());
        app.use(validator());
        app.use(express.static("public"));
        app.use(bodyParser.json());
        app.use(bodyParser.text({ type: "text/plain" }));
        app.use(queryParser());
        app.use(logger());
        app.use("/api/v1", basicRouter.get());
        app.use(errorHandler());
        this.app = app;
    }

    public get = () =>
       this.app
}
