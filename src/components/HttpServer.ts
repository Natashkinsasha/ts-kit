import * as http from "http";
import * as config from "node-config-env-value";

import App from "../components/App";
import logger from "../components/logger";

export default class HttpServer {

    private readonly server: any;
    private readonly port: number;

    public constructor({app}: { app: App }) {
        this.server = http.createServer(app.get());
        this.port = config.get("PORT");
    }

    public start = () => {
        this.server.listen(this.port, () => {
            logger.log("state", `Server start on ${this.port}`);
        });
    }
}
