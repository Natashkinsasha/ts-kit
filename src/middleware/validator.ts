import * as expressValidator from "express-validator";
import { ObjectId } from "mongodb";

export default () => expressValidator({
    customValidators: {
        isArrayObjectId: (value: string[]): boolean => value.reduce((result: boolean, iteam: string) => result && ObjectId.isValid(iteam), true),
        isNotNull: (value: string | boolean | number): boolean => value !== null,
        isObjectId: (value: string): boolean => ObjectId.isValid(value),
    },
});
