import * as expressWinston from "express-winston";
import logger from "../components/logger";

export default () => expressWinston.logger({
    winstonInstance: logger,
});
