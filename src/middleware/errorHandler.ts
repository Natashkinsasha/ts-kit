import * as Promise from "bluebird";
import * as express from "express";
import * as shortid from "shortid";
import logger from "../components/logger";
import ConsistencyError from "../error/ConsistencyError";
import ValidationError from "../error/ValidationError";

export default () => (err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    const failureId = shortid.generate();

    return Promise
        .reject(err)
        .catch(ValidationError, (err: ValidationError) =>
            res.status(400).json({
                code: err.code,
                failureId,
                message: err.message,
                name: err.name,
                result: err.result,
            }))
        .catch(ConsistencyError, (err: ConsistencyError) =>
            res.status(520).json({
                code: err.code,
                failureId,
                message: err.message,
                name: err.name,
            }))
        .then(() => {
            logger.warn(JSON.stringify({
                code: err.code,
                failureId,
                message: err.message,
                name: err.name,
                result: err.result,
                stack: err.stack,
            }));
        })
        .catch((err: Error & { code: string }) => {
            logger.error(JSON.stringify({
                code: err.code,
                failureId,
                message: err.message,
                name: err.name,
                stack: err.stack,
            }));
            return res.status(500).json({
                code: err.code,
                failureId,
                message: err.message,
                name: err.name,
            });
        });
};
