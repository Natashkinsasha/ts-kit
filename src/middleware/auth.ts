import * as Bluebird from "bluebird";
import * as jwt from "jsonwebtoken";
import * as config from "node-config-env-value";
import { compose, intersection, isEmpty } from "ramda";
import AuthError from "../error/AuthError";
import errorCodes from "../error/errorCodes";
import Payload from "../model/Payload";

const secretKey = config.get("AUTH.SECRET_KEY");
const header = config.get("AUTH.HEADER");

export default (...roles: string[]) => (req: any, res: any, next: any) =>
    Bluebird
        .try(() => jwt.verify(req.headers[header], secretKey))
        .then((payload: Payload) => {
            req.payload = payload;
            if (isEmpty(roles)) {
                return next();
            }
            if (payload.user && payload.user.roles && !compose(isEmpty, intersection(roles))(payload.user.roles)) {
                return next();
            }
            return next(new AuthError({code: errorCodes.NOT_ACCESS}));
        })
        .catch(next);
